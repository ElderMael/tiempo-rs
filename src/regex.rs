use regex::Regex;

use crate::error::{Error, Result};

pub fn parse_regex(s: &str) -> Result<Regex> {
    Regex::new(s).map_err(|_| Error::InvalidRegex(s.into()))
}
