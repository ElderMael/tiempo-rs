use std::env;

use atty::Stream;
#[cfg(windows)]
use ansi_term::enable_ansi_support;

/// Reads the given environment variable and decides if its value is true or
/// false
fn bool_env(name: &str) -> bool {
    if let Some(value) = env::var_os(name) {
        !(value == "0" || value == "false" || value == "")
    } else {
        false
    }
}

#[derive(Clone, Default)]
pub struct Env {
    pub timetrap_config_file: Option<String>,
    pub supress_warming: bool,
    pub stdout_is_tty: bool,
    pub stderr_is_tty: bool,
}

impl Env {
    pub fn read() -> Env {
        #[cfg(windows)]
        let tty = enable_ansi_support().is_ok();
        #[cfg(not(windows))]
        let tty = true;

        Env {
            timetrap_config_file: env::var("TIMETRAP_CONFIG_FILE").ok(),
            supress_warming: bool_env("TIEMPO_SUPRESS_TIMETRAP_WARNING"),
            stdout_is_tty: tty && atty::is(Stream::Stdout),
            stderr_is_tty: tty && atty::is(Stream::Stderr),
        }
    }
}
