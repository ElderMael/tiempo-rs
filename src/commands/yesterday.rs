use std::convert::TryFrom;
use std::io::{BufRead, Write};

use clap::ArgMatches;
use chrono::{Utc, Local, Duration};
use regex::Regex;

use crate::error::{Result, Error};
use crate::database::Database;
use crate::formatters::Formatter;
use crate::regex::parse_regex;
use crate::io::Streams;

use super::{Command, Facts, display::{Sheet, entries_for_display}};

#[derive(Default)]
pub struct Args {
    ids: bool,
    format: Option<Formatter>,
    grep: Option<Regex>,
    sheet: Option<Sheet>,
}

impl<'a> TryFrom<&'a ArgMatches<'a>> for Args {
    type Error = Error;

    fn try_from(matches: &'a ArgMatches) -> Result<Args> {
        Ok(Args {
            ids: matches.is_present("ids"),
            format: matches.value_of("format").map(|v| v.parse()).transpose()?,
            grep: matches.value_of("grep").map(parse_regex).transpose()?,
            sheet: matches.value_of("sheet").map(|s| s.parse()).transpose()?,
        })
    }
}

pub struct YesterdayCommand { }

impl<'a> Command<'a> for YesterdayCommand {
    type Args = Args;

    fn handle<D, I, O, E>(args: Self::Args, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let today = facts.now.with_timezone(&Local).date();
        let start = Some((today - Duration::days(1)).and_hms(0, 0, 0).with_timezone(&Utc));
        let end = Some(today.and_hms(0, 0, 0).with_timezone(&Utc));

        entries_for_display(
            start,
            end,
            args.sheet,
            streams,
            args.format.unwrap_or_else(|| facts.config.default_formatter.clone()),
            args.ids,
            args.grep,
            facts
        )
    }
}

#[cfg(test)]
mod tests {
    use chrono::{Duration, TimeZone};
    use pretty_assertions::assert_eq;

    use crate::config::Config;

    use super::*;

    #[test]
    fn returns_yesterday_entries_only() {
        let args = Args {
            format: Some(Formatter::Csv),
            ..Default::default()
        };
        let mut streams = Streams::fake(b"");
        let two_days_ago = Local::now().date() - Duration::days(2);
        let yesterday = Local::now().date() - Duration::days(1);
        let today = Local::now().date();
        let facts = Facts::new();

        streams.db.entry_insert(two_days_ago.and_hms(1, 2, 3).with_timezone(&Utc), None, None, "default").unwrap();
        streams.db.entry_insert(yesterday.and_hms(1, 2, 3).with_timezone(&Utc), None, Some("This!".into()), "default").unwrap();
        streams.db.entry_insert(today.and_hms(1, 2, 3).with_timezone(&Utc), None, None, "default").unwrap();

        YesterdayCommand::handle(args, &mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), &format!("start,end,note,sheet
{},,This!,default
", yesterday.and_hms(1, 2, 3).with_timezone(&Utc).to_rfc3339_opts(chrono::SecondsFormat::Micros, true)));

        assert_eq!(
            String::from_utf8_lossy(&streams.err),
            String::new(),
        );
    }

    #[test]
    fn respect_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Default::default();
        let mut streams = Streams::fake(b"");
        let now = Utc.ymd(2021, 7, 1).and_hms(10, 0, 0);
        let facts = Facts::new().with_config(Config {
            default_formatter: Formatter::Ids,
            ..Default::default()
        }).with_now(now);

        streams.db.entry_insert(Utc.ymd(2021, 6, 30).and_hms(10, 0, 0), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.ymd(2021, 6, 30).and_hms(10, 10, 0), None, Some("hola".into()), "default").unwrap();

        YesterdayCommand::handle(args, &mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }
}
