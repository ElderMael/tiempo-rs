use std::convert::TryFrom;
use std::io::{BufRead, Write};
use std::str::FromStr;

use clap::ArgMatches;
use chrono::{DateTime, Utc, Local, Datelike, TimeZone};
use regex::Regex;

use crate::error::{Result, Error};
use crate::database::Database;
use crate::formatters::Formatter;
use crate::regex::parse_regex;
use crate::io::Streams;

use super::{Command, Facts, display::{Sheet, entries_for_display}};

/// Given a local datetime, returns the time when the month it belongs started
fn beginning_of_month(time: DateTime<Local>) -> DateTime<Utc> {
    time.date().with_day(1).unwrap().and_hms(0, 0, 0).with_timezone(&Utc)
}

/// Given a datetime compute the time where the previous_month started in UTC
fn beginning_of_previous_month(time: DateTime<Local>) -> DateTime<Utc> {
    match time.month() {
        1 => {
            Local.ymd(time.year()-1, 12, 1).and_hms(0, 0, 0).with_timezone(&Utc)
        }
        n => Local.ymd(time.year(), n-1, 1).and_hms(0, 0, 0).with_timezone(&Utc)
    }
}

enum MonthSpec {
    Last,
    This,
    Month(u32),
}

impl Default for MonthSpec {
    fn default() -> MonthSpec {
        MonthSpec::This
    }
}

impl FromStr for MonthSpec {
    type Err = Error;

    fn from_str(s: &str) -> Result<MonthSpec> {
        match s.trim().to_lowercase().as_str() {
            "this" | "current" => Ok(MonthSpec::This),
            "last" => Ok(MonthSpec::Last),
            "jan" | "january" => Ok(MonthSpec::Month(1)),
            "feb" | "february" => Ok(MonthSpec::Month(2)),
            "mar" | "march" => Ok(MonthSpec::Month(3)),
            "apr" | "april" => Ok(MonthSpec::Month(4)),
            "may" => Ok(MonthSpec::Month(5)),
            "jun" | "june" => Ok(MonthSpec::Month(6)),
            "jul" | "july" => Ok(MonthSpec::Month(7)),
            "aug" | "august" => Ok(MonthSpec::Month(8)),
            "sep" | "september" => Ok(MonthSpec::Month(9)),
            "oct" | "october" => Ok(MonthSpec::Month(10)),
            "nov" | "november" => Ok(MonthSpec::Month(11)),
            "dic" | "december" => Ok(MonthSpec::Month(12)),
            _ => Err(Error::InvalidMonthSpec(s.into())),
        }
    }
}

#[derive(Default)]
pub struct Args {
    ids: bool,
    month: MonthSpec,
    format: Option<Formatter>,
    grep: Option<Regex>,
    sheet: Option<Sheet>,
}

impl<'a> TryFrom<&'a ArgMatches<'a>> for Args {
    type Error = Error;

    fn try_from(matches: &'a ArgMatches) -> Result<Args> {
        Ok(Args {
            ids: matches.is_present("ids"),
            month: matches.value_of("month").map(|s| s.parse()).transpose()?.unwrap_or(MonthSpec::This),
            format: matches.value_of("format").map(|v| v.parse()).transpose()?,
            grep: matches.value_of("grep").map(parse_regex).transpose()?,
            sheet: matches.value_of("sheet").map(|s| s.parse()).transpose()?,
        })
    }
}

pub struct MonthCommand { }

impl<'a> Command<'a> for MonthCommand {
    type Args = Args;

    fn handle<D, I, O, E>(args: Self::Args, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let now = facts.now;
        let (start, end) = match args.month {
            MonthSpec::This => (beginning_of_month(now.with_timezone(&Local)), now),
            MonthSpec::Last => {
                (beginning_of_previous_month(now.with_timezone(&Local)), beginning_of_month(now.with_timezone(&Local)))
            },
            MonthSpec::Month(month) => {
                if month < now.month() {
                    // the specified month is in the current year
                    (
                        Local.ymd(now.year(), month, 1).and_hms(0, 0, 0).with_timezone(&Utc),
                        if month < 12 {
                            Local.ymd(now.year(), month+1, 1).and_hms(0, 0, 0).with_timezone(&Utc)
                        } else {
                            Local.ymd(now.year()+1, 1, 1).and_hms(0, 0, 0).with_timezone(&Utc)
                        }
                    )
                } else {
                    // use previous year
                    (
                        Local.ymd(now.year() - 1, month, 1).and_hms(0, 0, 0).with_timezone(&Utc),
                        if month < 12 {
                            Local.ymd(now.year() - 1, month + 1, 1).and_hms(0, 0, 0).with_timezone(&Utc)
                        } else {
                            Local.ymd(now.year(), 1, 1).and_hms(0, 0, 0).with_timezone(&Utc)
                        }
                    )
                }
            },
        };

        entries_for_display(
            Some(start),
            Some(end),
            args.sheet,
            streams,
            args.format.unwrap_or_else(|| facts.config.default_formatter.clone()),
            args.ids,
            args.grep,
            facts
        )
    }
}

#[cfg(test)]
mod tests {
    use crate::config::Config;

    use super::*;

    #[test]
    fn respect_default_formatter() {
        std::env::set_var("TZ", "CST+6");

        let args = Default::default();
        let mut streams = Streams::fake(b"");
        let now = Utc.ymd(2021, 6, 30).and_hms(11, 0, 0);
        let facts = Facts::new().with_config(Config {
            default_formatter: Formatter::Ids,
            ..Default::default()
        }).with_now(now);

        streams.db.entry_insert(Utc.ymd(2021, 6, 30).and_hms(10, 0, 0), None, Some("hola".into()), "default").unwrap();
        streams.db.entry_insert(Utc.ymd(2021, 6, 30).and_hms(10, 10, 0), None, Some("hola".into()), "default").unwrap();

        MonthCommand::handle(args, &mut streams, &facts).unwrap();

        assert_eq!(&String::from_utf8_lossy(&streams.out), "1 2\n");
        assert_eq!(String::from_utf8_lossy(&streams.err), "");
    }
}
