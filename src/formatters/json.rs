use std::io::Write;

use crate::models::Entry;
use crate::error::Result;

pub fn print_formatted<W: Write>(entries: Vec<Entry>, out: &mut W) -> Result<()> {
    Ok(serde_json::to_writer(out, &entries)?)
}
