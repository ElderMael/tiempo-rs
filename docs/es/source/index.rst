Documentación de Tiempo
=======================

.. toctree::
   :maxdepth: 2
   :caption: Contenidos:

   basic_usage
   advanced_usage

Tiempo es una herramienta de línea de comandos para rastrear el tiempo. Te ayuda a grabar
el tiempo empleado en diferentes actividades y de manera opcional te permite organizarlas
en proyectos o `sheets` (hojas). Tiempo es compatible con `Timetrap`_, su predecesor.

Instalación
-----------

Si tienes instaladas las herramientas de `Rust`_, solo ejecuta:

.. code:: bash

    cargo install tiempo

Si usas Arch Linux, instala el paquete `tiempo-git` desde `AUR`_, por ejemplo:

.. code:: bash

   git clone https://aur.archlinux.org/tiempo-git.git && cd tiempo-git && makepkg -si

.. NOTE::
   Requieres tener instalado `Rust`_ y `Cargo`_. Para más información haz `clic aquí`_.

Inicio rápido
-------------

Ve `aquí`_...

Agradecimientos especiales
--------------------------

A `Timetrap`_ por existir y a `samg`_ por hacerlo. Esta herramienta es lo que estaba
buscando y a la que tomé de referencia, manteniendo compatibilidad en lo posible.

.. _Timetrap: https://github.com/samg/timetrap/
.. _samg: https://github.com/samg
.. _aquí: basic_usage.html#inicio-rapido
.. _clic aquí: https://doc.rust-lang.org/cargo/getting-started/installation.html
.. _Rust: https://rust-lang.org
.. _Cargo: https://doc.rust-lang.org/book/ch01-03-hello-cargo.html
.. _AUR: https://aur.archlinux.org/packages/tiempo-git/
