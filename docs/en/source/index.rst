Tiempo docs
===========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   basic_usage
   advanced_usage

Tiempo is a command line time tracker. It helps you keep track of the time spent
in different activities optionally organized in projects or `sheets`. Tiempo is
also compatible with `Timetrap`_, its predecesor.

Installation
------------

Provided you have a `Rust`_ toolchain installed, just do:

.. code:: bash

    cargo install tiempo

If you use Arch Linux, install ``tiempo-git`` package from the `AUR`_, for
example:

.. code:: bash

   git clone https://aur.archlinux.org/tiempo-git.git && cd tiempo-git && makepkg -si

.. NOTE::
   You need to install `Rust`_ and `Cargo`_. For more info `click here`_.

Quickstart
----------

Go `here`_...

Special Thanks
--------------

To `Timetrap`_ for existing, to `samg`_ for creating it. It is the tool I was
looking for and whose design I took as reference, keeping compatibility when
possible.

.. _Timetrap: https://github.com/samg/timetrap/
.. _samg: https://github.com/samg
.. _here: basic_usage.html#quickstart
.. _click here: https://doc.rust-lang.org/cargo/getting-started/installation.html
.. _Rust: https://rust-lang.org
.. _Cargo: https://doc.rust-lang.org/book/ch01-03-hello-cargo.html
.. _AUR: https://aur.archlinux.org/packages/tiempo-git/
