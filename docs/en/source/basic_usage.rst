Intro
=====

What is Tiempo
--------------

Tiempo is a command line time tracker. It helps you keep track of the time spent
in different activities optionally organized in projects or `sheets`. Tiempo is
also a `Timetrap`_ compatible command line time tracking application.

    Why another time tracking instead of improving Timetrap?

* Timetrap is `hard to install`_, hard to keep `updated`_ (because of Ruby).
  With Tiempo you can get or build a binary, put it somewhere, and it will
  just work forever in that machine. I'm bundling SQLite.
* Timetrap is slow (no way around it, because of Ruby), some commands take up to
  a second. Tiempo always feels snappy.
* Timetrap needed major refactor to fix the timezone problem (in a language I'm not
  proficient with). I was aware of this problem and designed Tiempo to store
  timestamps in UTC while at the same time being able to work with a database
  made by Timetrap without messing up. And there are a lot of tests backing this
  assertions.

Tiempo has other advantages:

* Fixed some input inconsistencies.
* Solved some old issues in Timetrap.
* Columns in the output are always aligned.
* CLI interface is easier to discover (ask ``-h`` for any sub-command).
* End times are printed with +1d to indicate that the activity ended the next
  day in the 'text' formatter.

Installation
------------

Just do:

.. code:: bash

    cargo install tiempo

If you use Arch Linux, install `tiempo-git` package from the `AUR`_, for example:

.. code:: bash

   git clone https://aur.archlinux.org/tiempo-git.git && cd tiempo-git && makepkg -si

.. NOTE::
   You need to install `Rust`_ and `Cargo`_. For more info `click here`_.

Quickstart
----------

TODO

.. _Timetrap: https://github.com/samg/timetrap/
.. _hard to install: https://github.com/samg/timetrap/issues/176
.. _updated: https://github.com/samg/timetrap/issues/174
.. _click here: https://doc.rust-lang.org/cargo/getting-started/installation.html
.. _Rust: https://rust-lang.org
.. _Cargo: https://doc.rust-lang.org/book/ch01-03-hello-cargo.html
.. _AUR: https://aur.archlinux.org/packages/tiempo-git/
