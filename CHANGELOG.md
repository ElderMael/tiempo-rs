# Changes

## 1.3.0

- Archive entries by the total sum of hours passing t-archive the --time
  argument.

## 1.2.5

- From now on AUR packages (tiempo-git and tiempo-bin) are updated from CI
  (yay!)
- Upgrade dependency rusqlite to 0.28 because 0.25 was yanked (perhaps a
  security issue)

## 1.2.0

- Add `--interactive` to `resume` subcommand

## 1.1.0

- Custom formatters

## 1.0.0

- First functional release with most features already!
