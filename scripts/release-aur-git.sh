#!/bin/bash

# Needed variables
VERSION=${CI_COMMIT_TAG:1}
PROJECT_NAME=tiempo
PROJECT_BINARY=t

# clone the repo
git clone $GIT_REPO_URL $PROJECT_NAME-git

# enter it
cd $PROJECT_NAME-git

echo "# Maintainer: Abraham Toriz <categulario at gmail dot com>
pkgname=$PROJECT_NAME-git
pkgver=$VERSION
pkgrel=1
pkgdesc='A command line time tracking application'
arch=('i686' 'x86_64')
url='https://gitlab.com/categulario/tiempo-rs'
license=('GPL3')
depends=()
optdepends=('sqlite: for manually editing the database')
makedepends=('cargo' 'git')
provides=('$PROJECT_NAME')
conflicts=('$PROJECT_NAME')
source=('$PROJECT_NAME-git::git+https://gitlab.com/categulario/tiempo-rs')
sha256sums=('SKIP')

pkgver() {
    cd \"\$pkgname\"
    printf \"%s\" \"\$(git describe --long | sed 's/\([^-]*-\)g/r\1/;s/-/./g;s/v//g')\"
}

build() {
    cd \"\$pkgname\"
    cargo build --release --locked
}

package() {
    cd \"\$pkgname\"
    install -Dm755 target/release/$PROJECT_BINARY \"\$pkgdir\"/usr/bin/$PROJECT_BINARY

    install -Dm644 README.md \"\$pkgdir\"/usr/share/doc/$PROJECT_NAME/README.md
    install -Dm644 LICENSE \"\$pkgdir\"/usr/share/doc/$PROJECT_NAME/LICENSE
    install -Dm644 CHANGELOG.md \"\$pkgdir\"/usr/share/doc/$PROJECT_NAME/CHANGELOG.md
}" | tee PKGBUILD > /dev/null

makepkg --printsrcinfo > .SRCINFO
git add .
git commit -m "Release version $VERSION"
git push
